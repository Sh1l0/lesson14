import Card from './Card.js';
import Game from './Game.js';
import SpeedRate from './SpeedRate.js';
import Creature from './Creature.js';

// Отвечает является ли карта уткой.
function isDuck(card) {
  return card instanceof Duck;
}

// Отвечает является ли карта собакой.
function isDog(card) {
  return card instanceof Dog;
}

// Дает описание существа по схожести с утками и собаками
export default function getCreatureDescription(card) {
  if (isDuck(card) && isDog(card)) {
      return 'Утка-Собака';
  }
  if (isDuck(card)) {
      return 'Утка';
  }
  if (isDog(card)) {
      return 'Собака';
  }
  return 'Существо';
}



// Основа для утки.
class Duck extends Creature {
  constructor() {
    super('Мирная утка', 2);
  }
  quacks () {
     console.log('quack');
  };
  swims () {
    console.log('float: both;');
  };
}


// Основа для собаки.
class Dog extends Creature {
  constructor(name = 'Пес-бандит', maxPower = 3) {
    super(name, maxPower);
  }
  swims () {
    console.log('float: none;');
  };
}


class Lad extends Dog {
  constructor() {
    super('Браток', 2);
  }
  static inGameCount = 0;
  static getInGameCount() {
    return this.inGameCount || 0;
  }
  static setInGameCount(value) {
    this.inGameCount = value;
  }
  static getBonus () {
    console.log(this.getInGameCount() * (this.getInGameCount() + 1) / 2);
    return this.getInGameCount() * (this.getInGameCount() + 1) / 2;
  }
  static updateCount (isAddScore) {
    if(isAddScore) {
      this.setInGameCount(this.getInGameCount() + 1);
      return;
    }
    this.setInGameCount(this.getInGameCount() - 1);
    return;
  }
  getDescriptions () {
    const arr = super.getDescriptions();
    arr.push('Чем их больше, тем они сильнее');
    //console.log(arr);
    //['Чем их больше, тем они сильнее']
    return arr;
  }
  doAfterComingIntoPlay(gameContext, continuation) {
    Lad.updateCount(true);
    console.log(Lad.inGameCount);
    continuation();
  }
  doBeforeRemoving (continuation) {
    Lad.updateCount(false);
    continuation();
  }
  modifyDealedDamageToCreature (value, toCard, gameContext, continuation) {
    super.modifyDealedDamageToCreature(value + Lad.getBonus(), toCard, gameContext, continuation)
  }
  modifyDealedDamageToPlayer (value, gameContext, continuation) {
    continuation(value);
  }
  modifyTakenDamage (value, fromCard, gameContext, continuation) {
    super.modifyTakenDamage(value - Lad.getBonus(), fromCard, gameContext, continuation);
  }
}

class Rogue extends Creature {
  constructor () {
    super('Изгой', 2);
  }
  doBeforeAttack (gameContext, continuation) {  //DO before attack
    const {currentPlayer, oppositePlayer, position, updateView} = gameContext;
    const oppositeCard = oppositePlayer.table[position];
    console.log(oppositeCard, oppositePlayer.deck, gameContext);
    const cardPrototype = Object.getPrototypeOf(oppositeCard);
    const properties = ['modifyDealedDamageToCreature', 'modifyTakenDamage', 'modifyDealedDamageToPlayer'];
    for(let prop of properties) {
      if(cardPrototype.hasOwnProperty(prop)){
        this[prop] = cardPrototype[prop];
        delete cardPrototype[prop];
      }
    }
     //context
    updateView();
    continuation();
  }
}
// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
  new Duck(),
  new Duck(),
  new Duck(),
  new Rogue()

];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
  new Lad(),
  new Lad(),
  new Lad()
];


// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
  alert('Победил ' + winner.name);
});
