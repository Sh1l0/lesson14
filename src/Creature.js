import Card from './Card.js';
import getCreatureDescription from './index.js';

class Creature extends Card {
  constructor (name, strength) {
    super(name, strength);
  }
  getDescriptions () {
    console.log(super.getDescriptions(), getCreatureDescription(this))
    return [super.getDescriptions(), getCreatureDescription(this)];
  }
}

export default Creature;
